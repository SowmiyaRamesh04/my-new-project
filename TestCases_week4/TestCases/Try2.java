package TestCases;

import java.util.concurrent.TimeUnit;

public class Try2 {

	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		//code
		long endTime = System.currentTimeMillis();
		System.out.println("Took "+(endTime - startTime) + " ns");
		
		long elapsedTime = endTime - startTime;
		/*double seconds = (double)elapsedTime;
		System.out.println("Took "+(seconds) + " se");
*/
		long milliseconds = elapsedTime;

        // long minutes = (milliseconds / 1000) / 60;
        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);

        // long seconds = (milliseconds / 1000);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

        System.out.format("%d Milliseconds = %d minutes\n", milliseconds, minutes );
        System.out.println("Or");
        System.out.format("%d Milliseconds = %d seconds", milliseconds, seconds );
		
		
	}

}
