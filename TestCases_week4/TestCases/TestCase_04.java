package TestCases;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestCase_04 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		// TestCase 01				
				System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				driver.get("http://leaftaps.com/opentaps/");
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				driver.manage().window().maximize();//maximize
				driver.findElementById("username").sendKeys("DemoSalesManager",Keys.TAB);
				driver.findElementById("password").sendKeys("crmsfa");
				driver.findElementByClassName("decorativeSubmit").click();
				driver.findElementByLinkText("CRM/SFA").click();
				driver.findElementByLinkText("Leads").click();
				
				//tc 04
				driver.findElementByLinkText("Merge Leads").click();
				
				// click src
				WebElement temp = driver.findElement(By.xpath("//img[@src='/images/fieldlookup.gif']"));
				temp.click();
				
				// move to new window
				Set<String> multiplewindow =driver.getWindowHandles();
				List<String> ls = new ArrayList();
				ls.addAll(multiplewindow);

				driver.switchTo().window(ls.get(1));
				System.out.println("second window Title.... "+driver.getTitle());
				
				driver.findElementByName("id");
				
				//click Find Leads
				driver.findElementByLinkText("Find Leads").click();

				
				

	}

}
