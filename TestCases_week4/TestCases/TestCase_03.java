package TestCases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TestCase_03 {

	/*public static void main(String[] args) {*/
		// TODO Auto-generated method stub
	
	@Test(dependsOnMethods= {"TestCases.TestCase_01"})
public void duplicate()
{
		// TestCase 01
		//Duplicate Lead
		
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//default size for browser
		//maximize the window //no minimize
		driver.manage().window().maximize();//maximize
		driver.findElementById("username").sendKeys("DemoSalesManager",Keys.TAB);
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		// click lead button
		driver.findElementByLinkText("Leads").click();
		//TestCase 02 Start--Duplicate Lead
		driver.findElementByLinkText("Find Leads").click();
		
		//enter first name
		//driver.findElementByName("firstName").sendKeys("sowmiya");
		//driver.findElementByXPath("//input[(@name='firstName')]").sendKeys("sowmiya");

		// Last name and click find lead
		driver.findElement(By.xpath("(//input[(@name='lastName')])[3]")).sendKeys("ramesh");
		//driver.findElementByName("lastName").sendKeys("ramesh");
		driver.findElementByClassName("//button[(text()='Find Leads')]").click();
		//driver.findElementByXPath("")
		
		driver.findElementByXPath("//a[(text()='10108')]").click();
		
		WebElement ele=	driver.findElement(By.xpath("//a[text()='Edit']"));
		ele.click();
		//changing company name
		driver.findElementById("updateLeadForm_companyName").sendKeys("NOVA");
	//click update
		driver.findElementByName("submitButton").click();
		//verify the title 
		String str = driver.getTitle();
		System.out.println("Title is...."+str);
		
		// verify tje updated company  name
		String UpdatedName = driver.findElementById("viewLead_companyName_sp").getText();
		if(UpdatedName.equals("NOVA"))
		{
			System.out.println("Value is same"+UpdatedName);
		}
		else
		{
			System.out.println("Value are not same");
		
		}
		
	}
	}
	
