package TestCases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class TestCase_01 {

	/*public static void main(String[] args)*/ 
	@Test(dependsOnMethods= {"bl.framework.testcases.TC_001_CreateLead"})
	public void CreateLead()
	{
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//default size for browser
		//maximize the window //no minimize
		driver.manage().window().maximize();//maximize
		driver.findElementById("username").sendKeys("DemoSalesManager",Keys.TAB);
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		// click lead button
		driver.findElementByLinkText("Leads").click();
		
		//Create Lead
		driver.findElementByPartialLinkText("Create Lead").click();
		
		//Getting FieldName 
		String str = driver.findElementByXPath("//span[@class='requiredField']").getText();
		System.out.println("Getting FieldName ..."+str);
		//field Value enter
		driver.findElementById("createLeadForm_companyName").sendKeys("ABC");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sowmiya");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ramesh");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("ABC");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("abc");
		driver.findElementById("createLeadForm_birthDate").sendKeys("01/12/2019");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("sowmiyaramesh0495@gmail.com");
		//click create lead button
		driver.findElementByName("submitButton").click();

	}

}
