package TestCases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestCase_02 {

	public static void main(String[] args) {

		// TestCase 01
		//Duplicate Lead
		
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//default size for browser
		//maximize the window //no minimize
		driver.manage().window().maximize();//maximize
		driver.findElementById("username").sendKeys("DemoSalesManager",Keys.TAB);
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		// click lead button
		driver.findElementByLinkText("Leads").click();
		
		//TestCase 02 Start--Duplicate Lead
		driver.findElementByLinkText("Find Leads").click();
		// click email
	//https://stackoverflow.com/questions/38534241/how-to-locate-a-span-with-a-specific-text-in-selenium-using-java		
		//enter email id one way
			
			//WebElement ele = driver.findElement(By.xpath("//span[.='Email']"));//NOT A PROPER WAY
			// another way
			WebElement ele = driver.findElement(By.xpath("//span[text()='Email']"));
			System.out.println("....."+ele.getText());
			ele.click();
			System.out.println("completed");
			driver.findElementByName("emailAddress").sendKeys("sowmiyaramesh0495@gmail.com");
			driver.findElementByXPath("//button[(text()='Find Leads')]").click();
			driver.findElementByPartialLinkText("Sowmiya").click();
			driver.findElementByXPath("//a[(text()='Duplicate Lead')]").click();
			
			String txt = driver.findElementByXPath("//div[(@class='x-panel-header sectionHeaderTitle')]").getText();
			if(txt.equals("Duplicate Lead"))
				
					{
		System.out.println("verified...."+txt);
					}
			else
			{
				System.out.println("not verfied...."+txt);
			}
	
			driver.findElementByXPath("//input[(@type='submit')]").click();
			driver.close();
	}
	
	}

