package Learning;
import java.awt.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class LearnIRCTC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		long startTime = System.nanoTime();
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Sowmiya");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Ramesh");
		driver.findElementById("userRegistrationForm:gender:1").click();
		System.out.println("completed");
	driver.findElementById("userRegistrationForm:maritalStatus:1").click();
	
		Select date = new Select(driver.findElementById("userRegistrationForm:dobDay"));
		//date.selectByVisibleText("04");
		//date.selectByIndex(3);
		date.selectByValue("04");
		Select month = new Select(driver.findElementById("userRegistrationForm:dobMonth"));
		month.selectByVisibleText("MAR");
		Select year = new Select(driver.findElementById("userRegistrationForm:dateOfBirth"));
		year.selectByVisibleText("1995");
		
		
		//https://loadfocus.com/blog/2016/06/13/how-to-select-a-dropdown-in-selenium-webdriver-using-java/--link
		//DATE OF BIRTH
		Select months = new Select(driver.findElement(By.id("userRegistrationForm:dobMonth")));
		 java.util.List<WebElement> elementCount = months.getOptions();
		 System.out.println(elementCount.size());
		 
		 /*//for each
		  * for (WebElement td: elementCount) 
		   {
		      System.out.println(td.getText());
		   }*/
		 // for iterator
		 for (int i = 0; i < elementCount.size(); i++) {
			 
			 System.out.println("Month..."+elementCount.get(i).getText());
			
		}
		 long endTime = System.nanoTime();
			System.out.println("Took "+(endTime - startTime) + " ns");
			

			long elapsedTime = endTime - startTime;
			long milliseconds = elapsedTime;

	        // long minutes = (milliseconds / 1000) / 60;
	        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);

	        // long seconds = (milliseconds / 1000);
	        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds);

	        System.out.format("%d Milliseconds = %d minutes\n", milliseconds, minutes );
	        System.out.println("Or");
	        System.out.format("%d Milliseconds = %d seconds", milliseconds, seconds );
		 // OCCUPATION
		// Select occupation = new Select(element) 

		}

		
		
	}