package Week4_Day2;

import java.io.IOException;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.stylesheets.LinkStyle;

import com.google.common.collect.Table;

import javafx.scene.control.Alert;


public class CommonMethods {

	public static void main(String[] args) throws IOException, InterruptedException {
		// TODO Auto-generated method stub

		
 //public void allmethods()

		//launch
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.manage().window().maximize();//maximize	
	// dropdown using select by visibile 
		WebElement el = driver.findElementById("");
		Select sc = new Select(el);
		sc.selectByVisibleText("");
		//dropdown using selectby value
		sc.selectByValue("");
		//dropdown using select by index
		sc.selectByIndex(4);
		
		// count of the option
		//get option is selct of list element
		java.util.List <WebElement> alloption = sc.getOptions();
		 int count= alloption.size();
		 
		 // read the text element
		 //All verification video  details
		 String text= driver.findElementById("").getText();
		 
		 //current url
		 driver.getCurrentUrl();
		 
		 
		 //verification checkbox is enable or not 
	boolean checkbox= 	 driver.findElementById("").isSelected();
		
		//code of window switch second window
	Set <String> allwindowset = driver.getWindowHandles();
	// another way to add all window ///pass value in constructor
	List <String> allwindowlist = new ArrayList(allwindowset);
	// add all window --one way
	// allwindowlist.addAll(allwindowset);
	driver.switchTo().window(allwindowlist.get(1));
	
	/// Snap Shot
	
	java.io.File src = driver.getScreenshotAs(OutputType.FILE);
     java.io.File des = new java.io.File("/snap/img.png");
   // Automatically create folder in Project
     //Third Party tool
     FileUtils.copyFile(src, des);
	
     //Method for alert
     //switching to alert
     org.openqa.selenium.Alert alert = driver.switchTo().alert();
     driver.switchTo().alert().accept();//directly can give like this
     alert.dismiss();
     driver.switchTo().alert().sendKeys("");
     driver.switchTo().alert().getText();
     
     // frames related code
     
     driver.switchTo().frame(0);
     driver.switchTo().frame("id or name");
     driver.switchTo().frame(driver.findElementById(""));
     driver.switchTo().defaultContent();// always go to super HTLM page i.e first  HTLM page
     driver.switchTo().parentFrame();
     
	
	// web table --- 4th row  3 column
     //Table 1 ---name eg
     WebElement table =  driver.findElementByXPath("");//class name or somthing related to particular 
     //row count start from 1 
     //column count start from index i.e 0
    List <WebElement> rows  = driver.findElements(By.tagName("tr"));
    List <WebElement> columns  = rows.get(3).findElements(By.tagName("td"));
     System.out.println(columns.get(2).getText());
     
     // wait concept
     //implicitlyWait--
     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
     //Explict wait
     Thread.sleep(3000);
    //Webdriver wait
     WebElement element= driver.findElementById("");		 
     WebDriverWait wait = new WebDriverWait(driver, 10);
     wait.until(ExpectedConditions.elementToBeClickable(element));
     
     
     //
    driver.findElementById("").sendKeys("");
    driver.findElementById("").sendKeys("");
    driver.findElementByXPath("").click();
    driver.findElementByClassName("").getAttribute("");
    driver.findElementByLinkText("").getText();
    driver.findElementByCssSelector("").getCssValue("");
	WebElement elename= driver.findElementByName("");
	
	}
	

}
