package week4_day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import javafx.scene.control.Alert;

public class LearnAlert {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
				//launch
						System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
						ChromeDriver driver = new ChromeDriver();
						//load URL
						driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
						//default size for browser
						//maximize the window //no minimize
						driver.manage().window().maximize();//maximize
						driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
					WebElement frameelement=	driver.findElementById("iframeResult");
					driver.switchTo().frame(frameelement);
				//iframe button
					driver.findElementByXPath("//button[text()='Try it']").click();
					// before click ok getting text from alert box
					org.openqa.selenium.Alert alertbox = driver.switchTo().alert();
				String text = alertbox.getText();
				System.out.println("Message from alert box..."+text);
				
				String str = "Hai !!!! How are you??";
				alertbox.sendKeys(str);
				alertbox.accept();
				// to cllick ok in alert
			//	alertbox.dismiss();
				// to get the g=text from iframe i.e iframe box		   // 	take messge from 
				String message =driver.findElementById("demo").getText();
				System.out.println("Message is"+message);
				if(message.contains("Hai"))
				{
					System.out.println("yes message contains");
				}
				else
				{
					System.err.println("not contain");
				}
				
					driver.switchTo().defaultContent();

	}

}
