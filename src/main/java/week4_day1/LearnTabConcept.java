package week4_day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnTabConcept {

	public static void main(String[] args) {
		
		//launch
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();//maximize
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		//contact us click
		driver.findElementByPartialLinkText("Contact").click();
		// multiple window 
		//to get the refference of particular window using set --set does not contain duplicate values
		//set does not hav get method
		//and also v cant get particular value in set 
		//so moving to List to get single value because list hav get method 
		Set<String> multiplewindow =driver.getWindowHandles();
		List<String> ls = new ArrayList();
		ls.addAll(multiplewindow);

		driver.switchTo().window(ls.get(1));
		System.out.println("second window Title.... "+driver.getTitle());

		driver.switchTo().window(ls.get(0));//switching to particular tab i.e window
		//Now driver obj 
		//System.out.println("First window Title.... "+driver.getTitle());
		driver.close();
		
	}

}
