import java.util.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class UrlLaunch {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//launch
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("http://leaftaps.com/opentaps/");
		//default size for browser
		//maximize the window //no minimize
		driver.manage().window().maximize();//maximize
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		//Create Lead
		driver.findElementByPartialLinkText("Create Lead").click();
		
		//Getting FieldName 
		String str = driver.findElementByXPath("//span[@class='requiredField']").getText();
		System.out.println(str);
		//field Value enter
		driver.findElementById("createLeadForm_companyName").sendKeys("ABC");
		driver.findElementById("createLeadForm_firstName").sendKeys("Sowmiya");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ramesh");
		driver.findElementById("createLeadForm_parentPartyId").sendKeys("123");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("ABC");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("abc");
		driver.findElementById("createLeadForm_birthDate").sendKeys("01/12/2019");
		
		
	//////select by visible method
		WebElement visibletxt=  driver.findElementById("createLeadForm_dataSourceId");
		Select vt = new Select(visibletxt);
		vt.selectByVisibleText("Employee");//select the value
		
   //////select by value method
		WebElement selectvalue=  driver.findElementById("createLeadForm_marketingCampaignId");
		Select st = new Select(selectvalue);
		st.selectByValue("DEMO_MKTG_CAMP");//select the value//
		
    //////select by  method
				WebElement industry=  driver.findElementById("createLeadForm_industryEnumId");
				Select os = new Select(industry);
				List<WebElement> industrylist = os.getOptions();
				System.out.println(industrylist.size()-1);
				os.selectByIndex(industrylist.size()-1);//print last before value////select the value
				String firsttxt = os.getFirstSelectedOption().getText();//last value
				System.out.println("///"+firsttxt);
			System.out.println();
				for (WebElement eachoption : industrylist)
				{
					//System.out.println("industry option values..."+eachoption.getText());
					if (eachoption.getText().startsWith("M"))
					{
						System.out.println("industry option Startwith M..."+eachoption.getText());
					}
					else
					{
					//	System.out.println("industry value does not Startwith M..."+eachoption.getText());
					}
					
				}
				
		
		
		
		
		
		
		
		
		
		//driver.findElementByClassName("smallSubmit").click();
		//Find element 
		
		
		
		
	}

}
