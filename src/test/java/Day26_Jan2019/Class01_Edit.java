package Day26_Jan2019;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;


public class Class01_Edit {

	public static void main(String[] args) {
	
		//launch
		System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		//load URL
		driver.get("http://www.leafground.com/pages/Edit.html");
		//default size for browser
		//maximize the window //no minimize
		driver.manage().window().maximize();//maximize
		driver.findElementById("email").sendKeys("sowmiyaramesh0495@gmail.com",Keys.TAB);
		driver.findElementByName("text").sendKeys("sowmi",Keys.TAB);

	}

}
