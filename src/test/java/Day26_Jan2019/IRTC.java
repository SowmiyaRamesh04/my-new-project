package Day26_Jan2019;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class IRTC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//launch
				System.setProperty("webdriver.chrome.driver", "./Driver\\chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				//load URL
				driver.get("https://erail.in/");
				//default size for browser
				//maximize the window //no minimize
				driver.manage().window().maximize();//maximize
				driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
				driver.findElementById("txtStationFrom").clear();
				driver.findElementById("txtStationFrom").sendKeys("Chennai Beach Jn",Keys.ENTER);
				driver.findElementById("txtStationTo").clear();
				driver.findElementById("txtStationTo").sendKeys("Bangalore East",Keys.ENTER);
				WebElement Dt = driver.findElementByXPath("//table[@ class='DataTable TrainList']");
				List<WebElement> DTrows = Dt.findElements(By.tagName("tr"));
				System.out.println("Total row count...."+DTrows.size());
				
			
				
				for(int i = 0; i<DTrows.size(); i++)
				{
					WebElement firstrow=DTrows.get(0);
					List<WebElement> columnNames = firstrow.findElements(By.tagName("td"));
					System.out.println("Names"+columnNames.get(i).getText());
				}
		
	}

}
